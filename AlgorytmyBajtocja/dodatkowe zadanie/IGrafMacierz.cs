﻿using System.Collections.Generic;

namespace dodatkowe_zadanie
{
    interface IGrafMacierz
    {
        List<List<wierzcholek>> Adj { get; set; }
        int LiczbaWierzcholkow { get; set; }
        bool czyJestKrawedzZVdoW(int v, int w);
        bool dajCzyIstniejeKrawedz(krawedz k);
        void dodajKrawedz(krawedz k, bool dezaktywator = false);
        void dodajWierzcholek();
        void generatorGrafu(int zadanaLiczbaKrawedzi);
        void generatorGrafu(double zadanaGestosc);
        int maksymalnaLiczbaKrawedz();
        void usunKrawedz(krawedz k);
        void usunWierzcholek();
        void Wypisz();
    }
}