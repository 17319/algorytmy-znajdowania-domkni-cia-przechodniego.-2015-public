﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace dodatkowe_zadanie
{
    class GrafMacierz : IGrafMacierz
    {
        List<List<wierzcholek>> adj;
        bool czyDigraf;
        int liczbaWierzcholkow;
        int liczbaKrawedzi;

        public int LiczbaWierzcholkow
        {
            get
            {
                return liczbaWierzcholkow;
            }

            set
            {
                liczbaWierzcholkow = value;
            }
        }

        public List<List<wierzcholek>> Adj
        {
            get
            {
                return adj;
            }

            set
            {
                adj = value;
            }
        }

        public GrafMacierz(int n, bool czyDigraf = true)
        {
            this.czyDigraf = czyDigraf;
            this.LiczbaWierzcholkow = n;
            this.liczbaKrawedzi = 0;
            adj = new List<List<wierzcholek>>();
            for (int i = 0; i < LiczbaWierzcholkow; i++)
            {
                adj.Add(new List<wierzcholek>());
            }
            wierzcholek w = new wierzcholek(false);
            for (int i = 0; i < LiczbaWierzcholkow; i++)
                for (int j = 0; j < LiczbaWierzcholkow; j++)
                {
                    adj[i].Add(w);
                }
        }

        internal List<int> zdajSasiadow(int v)
        {
            List<int> listaSasiadow = new List<int>();
            for (int i = 0; i < adj[v].Count; i++)
                if (adj[v][i].CzyIsteniejeKrawedz != false)
                    listaSasiadow.Add(i);
            return listaSasiadow;
        }

        public bool dajCzyIstniejeKrawedz(krawedz k)
        {
            return adj[k.Skad][k.Dokad].CzyIsteniejeKrawedz;
        }

        public void dodajKrawedz(krawedz k, bool dezaktywator = false)
        {
            int skad = k.Skad, dokad = k.Dokad;
            if (!adj[skad][dokad].CzyIsteniejeKrawedz)
            {
                liczbaKrawedzi++;
                adj[skad][dokad] = new wierzcholek(true);
            }
            if (!czyDigraf)
                adj[dokad][skad] = new wierzcholek(true);
        }

        public void usunKrawedz(krawedz k)
        {
            int skad = k.Skad, dokad = k.Dokad;
            if (adj[skad][dokad].CzyIsteniejeKrawedz)
            {
                liczbaKrawedzi--;
                adj[skad][dokad] = new wierzcholek(false);
            }
            if (!czyDigraf)
                adj[dokad][skad] = new wierzcholek(false);
        }

        public void dodajWierzcholek()
        {

            wierzcholek w = new wierzcholek(false);
            List<wierzcholek> ostatni = new List<wierzcholek>();
            for (int i = 0; i < LiczbaWierzcholkow + 1; i++)
            {
                ostatni.Add(w);
            }
            for (int i = 0; i < LiczbaWierzcholkow; i++)
                adj[i].Add(w);
            adj.Add(ostatni);
            LiczbaWierzcholkow++;
        }

        public void Wypisz()
        {
            Console.Write("Reprezentacja macierzowa:\n" + "   ");
            for (int i = 0; i < LiczbaWierzcholkow; i++)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
            for (int i = 0; i < LiczbaWierzcholkow; i++)
            {
                Console.Write(i + "  ");
                foreach (wierzcholek j in adj[i])
                {
                    bool czy = j.CzyIsteniejeKrawedz;
                    if (czy)
                        Console.Write("1 ");
                    else
                        Console.Write("0 ");
                }
                Console.WriteLine();
            }
        }

        public void usunWierzcholek()
        {
            for (int j = 0; j < LiczbaWierzcholkow; j++)
            {
                adj[j].RemoveAt(LiczbaWierzcholkow - 1);
            }
            LiczbaWierzcholkow--;
        }

        public void generatorGrafu(int zadanaLiczbaKrawedzi)
        {
            Random r = new Random();
            int v = 0, w = 0;
            for (int i = 0; i < zadanaLiczbaKrawedzi; i++)
            {

                v = r.Next(LiczbaWierzcholkow);
                w = r.Next(0, LiczbaWierzcholkow);
                if (!czyJestKrawedzZVdoW(v, w) && !czyJestKrawedzZVdoW(w, v) && v != w)
                {
                    dodajKrawedz(new krawedz(v, w));
                    //  Console.WriteLine(" " + i);
                    if (!czyDigraf)
                        dodajKrawedz(new krawedz(w, v));
                }
                else
                    --i;
            }
        }

        public bool czyJestKrawedzZVdoW(int v, int w)
        {
            if (adj[v][w].CzyIsteniejeKrawedz) return true;
            else return false;
        }

        public void generatorGrafu(double zadanaGestosc)
        {
            int max = maksymalnaLiczbaKrawedz();
            while (liczbaKrawedzi / (double)max < zadanaGestosc)
            {
                Random r = new Random();
                int v = r.Next(LiczbaWierzcholkow);
                int w = r.Next(0, LiczbaWierzcholkow);
                if (!czyJestKrawedzZVdoW(v, w) && !czyJestKrawedzZVdoW(w, v) && v != w)
                {
                    dodajKrawedz(new krawedz(v, w));
                    if (!czyDigraf)
                        dodajKrawedz(new krawedz(w, v));
                }
            }
            // Console.WriteLine("Utworzono: " + liczbaKrawedzi + " krawędzi");
        }

        public int maksymalnaLiczbaKrawedz()
        {
            if (czyDigraf)
                return liczbaKrawedzi * (liczbaKrawedzi - 1) / 2;
            else
                return liczbaKrawedzi * (liczbaKrawedzi - 1);
        }

    }

}
