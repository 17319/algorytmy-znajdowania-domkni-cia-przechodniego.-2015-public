﻿
using System;
using System.Collections.Generic;

namespace dodatkowe_zadanie
{

    class Program
    {
        static void Main(string[] args)
        {
            PobieraczGrafu pobieracz = new PobieraczGrafu("wejscie.txt");
            GrafMacierz gm = pobieracz.Gm;
            gm.Wypisz();     
            BajtocjaZadanieDodatkowe bajtacja = new BajtocjaZadanieDodatkowe(gm);
            bajtacja.znajdzDrogeDlaBajtazara();
            Console.WriteLine("\n----------");


            Console.ReadKey();
        }
    }
}
