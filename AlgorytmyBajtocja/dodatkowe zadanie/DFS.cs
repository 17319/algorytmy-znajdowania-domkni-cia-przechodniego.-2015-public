﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dodatkowe_zadanie
{
    class DFS
    {
        GrafMacierz graf;
        GrafMacierz graf2;
        List<int> listaPre;
        List<int> listaTopologiczna;
        List<int> listaPost;
        int licznikPost;
        int licznik;

        public List<int> ListaTopologiczna
        {
            get
            {
                return listaTopologiczna;
            }

            set
            {
                listaTopologiczna = value;
            }
        }

        public DFS(GrafMacierz graf)
        {
            this.graf = graf; ;
            graf2 = new GrafMacierz(0);
            listaPre = new List<int>();
            ListaTopologiczna = new List<int>();
            listaPost = new List<int>();
            licznik = 0;
            licznikPost = 0;
            for (int i = 0; i < graf.LiczbaWierzcholkow; i++)
            {
                listaPre.Add(-1);
                listaPost.Add(-1);
                ListaTopologiczna.Add(-1);
                graf2.dodajWierzcholek();
            }
        }
        public void dfs()
        {
            for (int i = 0; i < graf.LiczbaWierzcholkow; i++)
                if (listaPre[i] == -1)
                    dfsr(i);
        }

        public void dfsr(int v)
        {
            listaPre[v] = licznik++;
            List<int> listaSasiadow = graf.zdajSasiadow(v);
            Console.Write("\nNastępniki v=" + v + " to: ");
            for (int i = 0; i < listaSasiadow.Count(); i++)
                Console.Write(listaSasiadow[i] + " ");



            for (int i = 0; i < listaSasiadow.Count(); i++)
                if (listaPre[listaSasiadow[i]] == -1)
                    dfsr(listaSasiadow[i]);

            int numer = licznikPost++;
            listaPost[v] = numer;
            ListaTopologiczna[numer] = v;
        }
    }
}
