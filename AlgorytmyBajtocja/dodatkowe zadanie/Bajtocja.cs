﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dodatkowe_zadanie
{
    class BajtocjaZadanieDodatkowe
    {
        List<int> tablicaPrzeskokow;
        List<int> tablicaOrder;
        GrafMacierz grafMacierzowy;

        public BajtocjaZadanieDodatkowe(GrafMacierz grafMAcierzowy)
        {
            this.grafMacierzowy = grafMAcierzowy;
            this.tablicaPrzeskokow = new List<int>();
            for (int i = 0; i < this.grafMacierzowy.Adj.Count; i++)
                tablicaPrzeskokow.Add(-1);
        }

        public void znajdzDrogeDlaBajtazara()
        {
            DFS dfs = new DFS(grafMacierzowy);
            dfs.dfs();
            tablicaOrder = dfs.ListaTopologiczna;
            int maksimum = 0;
#if true
            Console.WriteLine("\n\ntablicaOrder");
            for (int i = 0; i < tablicaOrder.Count; i++)
                Console.Write(tablicaOrder[i] + " ");
            Console.WriteLine("\ntablicaOrder\n");
#endif

            tablicaPrzeskokow[tablicaOrder[0]] = 0;

            for (int i = 0; i < tablicaOrder.Count(); i++)
            {
                maksimum = int.MinValue;
                List<int> wskazowka = new List<int>(); ;
                wskazowka = grafMacierzowy.zdajSasiadow(tablicaOrder[i]);

                for (int j = 0; j < wskazowka.Count(); j++)
                    if (tablicaPrzeskokow[wskazowka[j]] >= maksimum)
                    {
                        maksimum = tablicaPrzeskokow[wskazowka[j]];
                        tablicaPrzeskokow[tablicaOrder[i]] = ++maksimum;
                    }
            }
#if true
            Console.Write("\nTablica przeskoków zawiera następujace wartości\n");
            for (int i = 0; i < tablicaPrzeskokow.Count(); i++)
                Console.Write(tablicaPrzeskokow[i] + " ");
            Console.Write("\nTablica przeskoków zawiera następujace wartości\n");
#endif

            Console.WriteLine("\n\nNajdłuższa możliwa trasa liczy " + (tablicaPrzeskokow.Max() + 1) + " miast");
            int odleglosc = 0;
            for (int i = 0; i < tablicaPrzeskokow.Count; i++)
            {
                if (tablicaPrzeskokow[i] != maksimum)
                    odleglosc++;
                else
                    break;
            }

            Console.WriteLine("Kolejność przemierzania miast: ");
            int licznik = maksimum;
            for (int i = 0; i <= maksimum + 1; i++)
            {
                List<int> listaSasiadow;
                listaSasiadow = grafMacierzowy.zdajSasiadow(odleglosc);
                if (listaSasiadow.Count() == 0)
                {
                    Console.Write("->" + odleglosc);
                    break;
                }
                for (int j = 0; j < listaSasiadow.Count(); j++)
                {
                    if (tablicaPrzeskokow[listaSasiadow[j]] == (licznik - 1))
                    {
                        Console.Write("->" + odleglosc);
                        odleglosc = listaSasiadow[j];
                        licznik--;
                        break;
                    }
                }
            }



        }
    }
}
