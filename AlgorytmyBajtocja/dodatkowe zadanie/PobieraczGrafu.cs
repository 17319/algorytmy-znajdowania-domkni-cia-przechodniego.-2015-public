﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace dodatkowe_zadanie
{
    class PobieraczGrafu
    {
        List<List<bool>> adj;
        GrafMacierz gm;

        public PobieraczGrafu(string nazwaPliku)
        {
           Gm = zrobGrafZMAcierzy(zczytajZPliku(nazwaPliku));
        }


        public List<List<bool>> Adj
        {
            get
            {
                return adj;
            }

            set
            {
                adj = value;
            }
        }

        internal GrafMacierz Gm
        {
            get
            {
                return gm;
            }

            set
            {
                gm = value;
            }
        }

        public List<List<bool>> zczytajZPliku(string nazwaPliku)
        {
            List<List<bool>> macierz = new List<List<bool>>();
            string[] tablicaStringow;
            StreamReader sR = new StreamReader(nazwaPliku);
            string linia = sR.ReadLine();
            tablicaStringow = linia.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int licznik = tablicaStringow.Count();
            for (int i = 0; i < licznik; i++)
            {
                linia = sR.ReadLine();
                tablicaStringow = linia.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                macierz.Add(new List<bool>());
                for (int j = 1; j <= licznik; j++)
                {
                    int g = int.Parse(tablicaStringow[j].Replace("\t", ""));
                    macierz[i].Add(g == 1 ? true : false);
                }
            }
            return macierz;
        }

        public GrafMacierz zrobGrafZMAcierzy(List<List<bool>> macierz)
        {
            GrafMacierz gm = new GrafMacierz(macierz.Count);
            for (int i = 0; i < macierz.Count; i++)
                for (int j = 0; j < macierz.Count; j++)
                    if (macierz[i][j])
                        gm.dodajKrawedz(new krawedz(i, j));
            return gm;
        }

    }
}