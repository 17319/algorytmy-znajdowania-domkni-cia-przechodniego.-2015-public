﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace projektbart_omiejkaftancs
{
    class Generator
    {
        GrafMacierz grafMacierzowy;
        GrafLista grafListowy;

        public Generator()
        {
            grafMacierzowy = new GrafMacierz(0);
            grafListowy = new GrafLista(0);
        }

        public GrafMacierz generujGrafMacierzowy(int liczbaWierzcholkow, int liczbaKrawedzi, int wielkoscJadra)
        {
            Random r = new Random();
            grafMacierzowy.dodajVWierzcholkow(liczbaWierzcholkow);
            int stosunekLiczbyWierzcholkowDoWielskosciJadra = liczbaWierzcholkow / wielkoscJadra;
            List<List<int>> listaPomocnicza = new List<List<int>>();
            int q = 0;
            for (int i = 0; i < wielkoscJadra; i++)
                for (int j = 0; j < stosunekLiczbyWierzcholkowDoWielskosciJadra; j++)
                {
                    listaPomocnicza.Add(new List<int>());
                    for (int p = q - j; p < liczbaWierzcholkow; p++)
                        listaPomocnicza[q].Add(p);
                    q++;
                }
            for (int a = 0; a < liczbaWierzcholkow; a++)
                listaPomocnicza[a].Insert(0, 1);

            for (int i = 0; i < wielkoscJadra; i++)
                for (int j = 0; j < stosunekLiczbyWierzcholkowDoWielskosciJadra; j++)
                    if (grafMacierzowy.LiczbaKrawedzi < liczbaKrawedzi)
                    {
                        int skad = j % stosunekLiczbyWierzcholkowDoWielskosciJadra + i * stosunekLiczbyWierzcholkowDoWielskosciJadra;
                        int dokad = (j + 1) % stosunekLiczbyWierzcholkowDoWielskosciJadra + i * stosunekLiczbyWierzcholkowDoWielskosciJadra;
                        grafMacierzowy.dodajKrawedz(new krawedz(skad, dokad));
                        int x = 0;
                        for (int y = 0; y < listaPomocnicza[skad].Count(); y++)
                            if (listaPomocnicza[skad][y] == dokad)
                            {
                                x = y;
                                break;
                            }
                        listaPomocnicza[skad].Remove(listaPomocnicza[skad][x]);
                        if (listaPomocnicza[skad].Count() == 1)
                            listaPomocnicza.Remove(listaPomocnicza[skad]);
                    }
            while (grafMacierzowy.LiczbaKrawedzi < liczbaKrawedzi)
            {
                if (listaPomocnicza.Count() == 0)
                    break;
                int pierwsza = r.Next() % (listaPomocnicza.Count());
                int druga = 1 + r.Next() % (listaPomocnicza[pierwsza].Count() - 1);
                grafMacierzowy.dodajKrawedz(new krawedz(listaPomocnicza[pierwsza][0], listaPomocnicza[pierwsza][druga]));
                listaPomocnicza[pierwsza].Remove(listaPomocnicza[pierwsza][druga]);
                if (listaPomocnicza[pierwsza].Count() == 1)
                    listaPomocnicza.Remove(listaPomocnicza[pierwsza]);
            }
           // grafMacierzowy.Wypisz();
          
            return grafMacierzowy;
        }


        public GrafLista generujGrafListowy(int liczbaWierzcholkow, int liczbaKrawedzi, int wielkoscJadra)
        {
            GrafMacierz gm = generujGrafMacierzowy(liczbaWierzcholkow, liczbaKrawedzi, wielkoscJadra);
            for (int i = 0; i < liczbaWierzcholkow; i++)
                grafListowy.dodajWierzcholek();

            for (int a = 0; a < liczbaWierzcholkow; a++)
                for (int b = 0; b < liczbaWierzcholkow; b++)
                    if (gm.czyJestKrawedzZVdoW(a, b))
                        grafListowy.dodajKrawedz(new krawedz(a, b));
           
            return grafListowy;
        }
















    }
}
