﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace projektbart_omiejkaftancs
{
    class GrafLista : IMetodyGrafowe
    {
        List<List<int>> adj;
        bool czyDigraf;
        int liczbaWierzcholkow;
        int liczbaKrawedzi;

        public int LiczbaWierzcholkow
        {
            get
            {
                return liczbaWierzcholkow;
            }

            set
            {
                liczbaWierzcholkow = value;
            }
        }

        public List<List<int>> Adj
        {
            get
            {
                return adj;
            }

            set
            {
                adj = value;
            }
        }

        public GrafLista(int n, bool czyDigraf = true)
        {
            adj = new List<List<int>>();
            for (int i = 0; i < n; i++)
            {
                adj.Add(new List<int>());
            }
            this.czyDigraf = czyDigraf;
            this.LiczbaWierzcholkow = n;
            this.liczbaKrawedzi = 0;
        }

        public bool czyJestKrawedzZVdoW(int v, int w)
        {
            if (adj[v].Contains(w))
                return true;
            else
                return false;
        }

        public void dodajKrawedz(krawedz k, bool dezaktywator = false)
        {
            if (dezaktywator || (!(czyJestKrawedzZVdoW(k.Skad, k.Dokad) || (czyJestKrawedzZVdoW(k.Dokad, k.Skad) && czyDigraf) || k.Skad == k.Dokad)))
            {
                if (!czyJestKrawedzZVdoW(k.Skad, k.Dokad))
                {
                    adj[k.Skad].Add(k.Dokad);
#if test
                    
                    
                    e.WriteLine("Dodano " + k.Skad + " -> " + k.Dokad); 
#endif
                    if (!czyDigraf)
                        adj[k.Dokad].Add(k.Skad);

                    liczbaKrawedzi++;
                } 
            }

        }

        public void dodajWierzcholek()
        {
            adj.Add(new List<int>());
            LiczbaWierzcholkow++;
        }

        public void generatorGrafu(double zadanaGestosc)
        {
            int max = maksymalnaLiczbaKrawedz();
            while (liczbaKrawedzi / (double)max < zadanaGestosc)
            {
                Random r = new Random();
                int v = r.Next(LiczbaWierzcholkow);
                int w = r.Next(0, LiczbaWierzcholkow);
                if (!czyJestKrawedzZVdoW(v, w) && !czyJestKrawedzZVdoW(w, v) && v != w)
                {
                    dodajKrawedz(new krawedz(v, w));
                    if (!czyDigraf)
                        dodajKrawedz(new krawedz(w, v));
                }
            }
           // Console.WriteLine("Utworzono: " + liczbaKrawedzi + " krawędzi");
        }

        public void generatorGrafu(int zadanaLiczbaKrawedzi)
        {
            for (int i = 0; i < zadanaLiczbaKrawedzi; i++)
            {
                Random r = new Random();
                int v = r.Next(LiczbaWierzcholkow);
                int w = r.Next(0, LiczbaWierzcholkow);
                if (!czyJestKrawedzZVdoW(v, w) && !czyJestKrawedzZVdoW(w, v) && v != w)
                {
                    dodajKrawedz(new krawedz(v, w));
                    if (!czyDigraf)
                        dodajKrawedz(new krawedz(w, v));
                }
                else
                    --i;
            }
        }

        public int maksymalnaLiczbaKrawedz()
        {
            if (czyDigraf)
                return LiczbaWierzcholkow * (LiczbaWierzcholkow - 1) / 2;
            else
                return LiczbaWierzcholkow * (LiczbaWierzcholkow - 1);
        }

        public void usunKrawedz(krawedz k)
        {
            if (czyJestKrawedzZVdoW(k.Skad, k.Dokad))
            {
                adj[k.Skad].Remove(k.Dokad);
                if (czyDigraf)
                    adj[k.Dokad].Remove(k.Skad);
                liczbaKrawedzi--;
            }
        }

        public void usunWierzcholek()
        {

            if (LiczbaWierzcholkow > 0)
            {
                adj.Remove(adj.Last());
                LiczbaWierzcholkow--;
            }
        }

        public void Wypisz()
        {
            Console.WriteLine("Reprezentacja listowa: ");
            for (int i = 0; i < LiczbaWierzcholkow; i++)
            {
                Console.Write(i + ": ");
                foreach (int j in adj[i])
                {
                    Console.Write(j + " ");
                }

                Console.WriteLine();
            }
        }
    }
}
