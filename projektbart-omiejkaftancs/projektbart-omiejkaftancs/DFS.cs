﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace projektbart_omiejkaftancs
{
    class DFS
    {
        GrafMacierz graf;
        GrafMacierz graf2;
        List<int> listaPre;
        List<int> listaTopologiczna;
        List<int> listaPost;
        int licznikPost;
        int licznik;

        public List<int> ListaTopologiczna
        {
            get
            {
                return listaTopologiczna;
            }

            set
            {
                listaTopologiczna = value;
            }
        }

        public List<int> ListaPre
        {
            get
            {
                return listaPre;
            }

            set
            {
                listaPre = value;
            }
        }

        public DFS(GrafMacierz graf)
        {
            this.graf = graf; ;
            graf2 = new GrafMacierz(0);

            ListaPre = new List<int>();
            ListaTopologiczna = new List<int>();
            listaPost = new List<int>();
            licznik = 0;
            licznikPost = 0;
            for (int i = 0; i < graf.LiczbaWierzcholkow; i++)
            {
                ListaPre.Add(-1);
                listaPost.Add(-1);
                ListaTopologiczna.Add(-1);
                graf2.dodajWierzcholek();
            }
        }
        public void dfs()
        {
            for (int i = 0; i < graf.LiczbaWierzcholkow; i++)
            {
                if (ListaPre[i] == -1)
                {
                    dfsr(i);
                }
            }
        }

        public void dfsr(int v)
        {
            ListaPre[v] = licznik++;

            List<int> B = graf.zdajSasiadow(v);

            //   Console.Write( "\nNastępniki v=" + v + " to: ");
            //for (int i = 0; i < B.Count(); i++)
            //    Console.Write(B[i] + " ");



            for (int i = 0; i < B.Count(); i++)
            {
                if (ListaPre[B[i]] == -1)
                {
                    dfsr(B[i]);
                }

            }

            int numer = licznikPost++;
            listaPost[v] = numer;
            ListaTopologiczna[numer] = v;
        }
    }
}
