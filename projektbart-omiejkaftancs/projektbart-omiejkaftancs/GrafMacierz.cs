﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace projektbart_omiejkaftancs
{
    class GrafMacierz : IMetodyGrafowe
    {
        List<List<wierzcholek>> adj;
        bool czyDigraf;
        int liczbaWierzcholkow;
        int liczbaKrawedzi;

        public int LiczbaWierzcholkow
        {
            get
            {
                return liczbaWierzcholkow;
            }

            set
            {
                liczbaWierzcholkow = value;
            }
        }

        public List<List<wierzcholek>> Adj
        {
            get
            {
                return adj;
            }

            set
            {
                adj = value;
            }
        }

        public int LiczbaKrawedzi
        {
            get
            {
                return liczbaKrawedzi;
            }

            set
            {
                liczbaKrawedzi = value;
            }
        }

        public GrafMacierz(int n, bool czyDigraf = true)
        {
            this.czyDigraf = czyDigraf;
            this.LiczbaWierzcholkow = n;
            this.LiczbaKrawedzi = 0;
            Adj = new List<List<wierzcholek>>();
            for (int i = 0; i < LiczbaWierzcholkow; i++)
            {
                Adj.Add(new List<wierzcholek>());
            }
            wierzcholek w = new wierzcholek(false);
            for (int i = 0; i < LiczbaWierzcholkow; i++)
                for (int j = 0; j < LiczbaWierzcholkow; j++)
                {
                    Adj[i].Add(w);
                }
        }

        internal List<int> dajSasiadow(int v)
        {
            throw new NotImplementedException();
        }

        public bool dajCzyIstniejeKrawedz(krawedz k)
        {
            return Adj[k.Skad][k.Dokad].CzyIsteniejeKrawedz;
        }
        
        public void dodajKrawedz(krawedz k, bool dezaktywator=false)
        {
            int skad = k.Skad, dokad = k.Dokad;
            if (!Adj[skad][dokad].CzyIsteniejeKrawedz)
            {
                LiczbaKrawedzi++;
                Adj[skad][dokad] = new wierzcholek(true);
            }
            if (!czyDigraf)
                Adj[dokad][skad] = new wierzcholek(true);
        }

        public void usunKrawedz(krawedz k)
        {
            int skad = k.Skad, dokad = k.Dokad;
            if (Adj[skad][dokad].CzyIsteniejeKrawedz)
            {
                LiczbaKrawedzi--;
                Adj[skad][dokad] = new wierzcholek(false);
            }
            if (!czyDigraf)
                Adj[dokad][skad] = new wierzcholek(false);
        }

        public void dodajWierzcholek()
        {

            wierzcholek w = new wierzcholek(false);
            List<wierzcholek> ostatni = new List<wierzcholek>();
            for (int i = 0; i < LiczbaWierzcholkow + 1; i++)
            {
                ostatni.Add(w);
            }
            for (int i = 0; i < LiczbaWierzcholkow; i++)
                Adj[i].Add(w);
            Adj.Add(ostatni);
            LiczbaWierzcholkow++;
        }

        public void Wypisz()
        {
            Console.Write("Reprezentacja macierzowa:\n" + "   ");
            for (int i = 0; i < LiczbaWierzcholkow; i++)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
            for (int i = 0; i < LiczbaWierzcholkow; i++)
            {
                Console.Write(i + "  ");
                foreach (wierzcholek j in Adj[i])
                {
                    bool czy = j.CzyIsteniejeKrawedz;
                    if (czy)
                        Console.Write("1 ");
                    else
                        Console.Write("0 ");
                }
                Console.WriteLine();
            }
        }

        public void dodajVWierzcholkow(int v)
        {
            for (int i = 0; i < v; i++)
            {
                liczbaWierzcholkow++;
                Adj.Add(new List<wierzcholek>());
                for (int j = 0; j < v; j++)
                {
                    Adj[i].Add(new wierzcholek(false));
                }
            }
        }

        public void usunWierzcholek()
        {
            for (int j = 0; j < LiczbaWierzcholkow; j++)
            {
                Adj[j].RemoveAt(LiczbaWierzcholkow - 1);
            }
            LiczbaWierzcholkow--;
        }

        public void generatorGrafu(int zadanaLiczbaKrawedzi)
        {
            for (int i = 0; i < zadanaLiczbaKrawedzi; i++)
            {
                Random r = new Random();
                int v = r.Next(LiczbaWierzcholkow);
                int w = r.Next(0, LiczbaWierzcholkow);
                if (!czyJestKrawedzZVdoW(v, w) && !czyJestKrawedzZVdoW(w, v) && v != w)
                {
                    dodajKrawedz(new krawedz(v, w));
                    if (!czyDigraf)
                        dodajKrawedz(new krawedz(w, v));
                }
                else
                    --i;
            }
        }

        public bool czyJestKrawedzZVdoW(int v, int w)
        {
            if (Adj[v][w].CzyIsteniejeKrawedz) return true;
            else return false;
        }

        public void generatorGrafu(double zadanaGestosc)
        {
            int max = maksymalnaLiczbaKrawedz();
            while (LiczbaKrawedzi / (double)max < zadanaGestosc)
            {
                Random r = new Random();
                int v = r.Next(LiczbaWierzcholkow);
                int w = r.Next(0, LiczbaWierzcholkow);
                if (!czyJestKrawedzZVdoW(v, w) && !czyJestKrawedzZVdoW(w, v) && v != w)
                {
                    dodajKrawedz(new krawedz(v, w));
                    if (!czyDigraf)
                        dodajKrawedz(new krawedz(w, v));
                }
            }
            // 
            Console.WriteLine("Utworzono: " + liczbaKrawedzi + " krawędzi");
        }

        public int maksymalnaLiczbaKrawedz()
        {
            if (czyDigraf)
                return LiczbaKrawedzi * (LiczbaKrawedzi - 1) / 2;
            else
                return LiczbaKrawedzi * (LiczbaKrawedzi - 1);
        }

        internal List<int> zdajSasiadow(int v)
        {
            List<int> listaSasiadow = new List<int>();
            for (int i = 0; i < adj[v].Count; i++)
                if (adj[v][i].CzyIsteniejeKrawedz != false)
                    listaSasiadow.Add(i);
            return listaSasiadow;
        }




    }

}
