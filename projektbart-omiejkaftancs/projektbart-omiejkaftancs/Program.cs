﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;


namespace projektbart_omiejkaftancs
{



    class Program
    {

        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            int liczbaKrawedzi = 0;
            int wielkoscJadra = 0;
            int maxLicznik = 0;
            int krok = 0;
            #region 1 wersja
            /*
            
            double gestosc = 0.1;
           
           

            #region GM_W
#if false
            maxLicznik = 1000;
            krok = 100;
            for (licznik = 0; licznik < maxLicznik; licznik += krok)
            {
                for (gestosc = 0.0; gestosc < 1.0; gestosc += 0.2)
                {
                    GrafMacierz gm = new GrafMacierz(licznik);
                    gm.generatorGrafu(gestosc);
                    sw.Start();
                    Warshal w = new Warshal(gm);
                    sw.Stop();
                    Console.WriteLine("GrafMacierz" + "\t" + "Warshal" + "\t" + gestosc + "\t" + licznik + "\t" + sw.ElapsedTicks);
                    sw.Reset();
                }
            }
#endif
            #endregion


            #region GL_W
#if false
            maxLicznik = 101;
            krok = 20;
            for (licznik = 0; licznik < maxLicznik; licznik += krok)
            {
                for (gestosc = 0.0; gestosc < 1.0; gestosc += 0.2)
                {
                    GrafLista gm = new GrafLista(licznik);
                    gm.generatorGrafu(gestosc);
                    sw.Start();
                    Warshal w = new Warshal(gm);
                    sw.Stop();
                    Console.WriteLine("GrafLista" + "\t" + "Warshal" + "\t" + gestosc + "\t" + licznik + "\t" + sw.ElapsedTicks);
                    sw.Reset();
                }
            }
#endif
            #endregion


            #region GM_DFS
#if true
            //maxLicznik = 11000;
            //krok = 1000;
            maxLicznik = 200;
            krok = 50;
            for (licznik = 0; licznik < maxLicznik; licznik += krok)
            {
                for (gestosc = 0.0; gestosc < 1.0; gestosc += 0.2)
                {
                    GrafMacierz gm = new GrafMacierz(licznik);
                    gm.generatorGrafu(gestosc);
                    sw.Start();
                    DFS w = new DFS(gm);
                    sw.Stop();
                    Console.WriteLine("GrafMacierz" + "\t" + "DFS" + "\t" + gestosc + "\t" + licznik + "\t" + sw.ElapsedTicks);
                    sw.Reset();
                }
            }
#endif
            #endregion


            #region GL_DFS
#if false
            maxLicznik = 200;
            krok = 50;
            for (licznik = 0; licznik < maxLicznik; licznik += krok)
            { 
                for (gestosc = 0.0; gestosc < 1.0; gestosc += 0.2)
                {
                    GrafLista gm = new GrafLista(licznik);
                    gm.generatorGrafu(gestosc);
                    sw.Start();
                    DFS w = new DFS(gm);
                    sw.Stop();
                    Console.WriteLine("GrafLista" + "\t" + "DFS" + "\t" + gestosc + "\t" + licznik + "\t" + sw.ElapsedTicks);
                    sw.Reset();
                }
            }
#endif
            #endregion 
             #endregion 
            */

            //Generator gen = new Generator();
            //GrafMacierz gm = gen.generujGrafMacierzowy(8, 8, 4);
            //gm = gen.generujGrafMacierzowy(10000, 10000, 1000);
            //    gm = gen.generujGrafMacierzowy(8, 8, 4);

            //  GrafLista gl = gen.generujGrafListowy(5, 7, 3 );
            // gl.Wypisz();
            #endregion




            DAG dag;
            Generator generator;
          
#if true
            Console.WriteLine("\nTesujemy DAG Macierz");
            maxLicznik = 10000;
            krok = 10;
           
            generator = new Generator();
            GrafMacierz grafMacierzowy;
             liczbaKrawedzi = 0;
             wielkoscJadra = 0;
            for (int liczbaWierzchołkow = 10; liczbaWierzchołkow < maxLicznik; liczbaWierzchołkow += krok)
                {
                liczbaKrawedzi = liczbaWierzchołkow * (liczbaWierzchołkow / 2);
                wielkoscJadra = (int)(liczbaWierzchołkow * 0.1);                    
                grafMacierzowy = generator.generujGrafMacierzowy(liczbaWierzchołkow, liczbaKrawedzi, wielkoscJadra);
                dag = new DAG(grafMacierzowy);
                dag.DAG_jadra();
                sw.Reset();
                sw.Start();
                dag.TC();
                sw.Stop();
                Console.WriteLine("GrafMacierz DAG " + liczbaWierzchołkow + " " + liczbaKrawedzi + " " + wielkoscJadra + " " + sw.ElapsedTicks);              
            }
#endif


#if false
            maxLicznik = 10000;
            krok = 10;
            generator = new Generator();
            GrafLista gl;
             liczbaKrawedzi = 0;
             wielkoscJadra = 0;
            for (int liczbaWierzchołkow = 10; liczbaWierzchołkow < maxLicznik; liczbaWierzchołkow += krok)
            {
                liczbaKrawedzi = liczbaWierzchołkow * (liczbaWierzchołkow/2);
                wielkoscJadra = (int)(liczbaWierzchołkow * 0.1);
                gl = generator.generujGrafListowy(liczbaWierzchołkow, liczbaKrawedzi, wielkoscJadra);
                dag = new DAG(gl);
                dag.DAG_jadra();
                sw.Reset();
                sw.Start();
                dag.TC();
                sw.Stop();
                Console.WriteLine("GrafListowy DAG " + liczbaWierzchołkow + " " + liczbaKrawedzi + " " + wielkoscJadra + " " + sw.ElapsedTicks);
            }
#endif












            Console.WriteLine(".......");
            Console.ReadKey();
        }


    }
}
