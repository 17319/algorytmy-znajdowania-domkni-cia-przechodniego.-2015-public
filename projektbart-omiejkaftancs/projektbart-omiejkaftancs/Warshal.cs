﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace projektbart_omiejkaftancs
{
    class Warshal
    {
        int liczbaWierzcholkow;
        public Warshal(GrafMacierz g, bool czyWypisac = false)
        {
            liczbaWierzcholkow = g.LiczbaWierzcholkow;
            for (int k = 0; k < liczbaWierzcholkow; k++)
                for (int i = 0; i < liczbaWierzcholkow; i++)
                    for (int j = 0; j < liczbaWierzcholkow; j++)
                        if (g.Adj[i][k].CzyIsteniejeKrawedz && g.Adj[k][j].CzyIsteniejeKrawedz)
                            g.Adj[i][j] = new wierzcholek(true);
            if (czyWypisac) g.Wypisz();
        }

        public Warshal(GrafLista g, bool czyWypisac = false)
        {
            liczbaWierzcholkow = g.LiczbaWierzcholkow;
            for (int k = 0; k < liczbaWierzcholkow; k++)
                for (int i = 0; i < liczbaWierzcholkow; i++)
                    for (int j = 0; j < liczbaWierzcholkow; j++)
                        if (g.czyJestKrawedzZVdoW(i, k) && g.czyJestKrawedzZVdoW(k, j))
                            g.dodajKrawedz(new krawedz(i, j), true);
            if (czyWypisac) g.Wypisz();
        }

    }
}
