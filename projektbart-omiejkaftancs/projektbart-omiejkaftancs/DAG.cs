﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace projektbart_omiejkaftancs
{
    class DAG
    {
        GrafMacierz grafMacierz;
        GrafLista grafLista;
        List<int> listaPre;
        List<int> nrSkladowej;
        List<int> listaOrder;
        int licznik;
        GrafMacierz grafMacierzA;
        GrafMacierz grafKosaraju;
        GrafMacierz grafDAG;

        public DAG(GrafMacierz grafMacierz)
        {
            this.grafMacierz = grafMacierz;
            listaPre = new List<int>();
            licznik = 0;
            grafMacierzA = new GrafMacierz(0);
            grafKosaraju = new GrafMacierz(0);
            for (int i = 0; i < grafMacierz.LiczbaWierzcholkow; i++)
            {
                listaPre.Add(-1);
                grafMacierzA.dodajWierzcholek();
                grafKosaraju.dodajWierzcholek();
            }
           
        }

        public DAG(GrafLista grafLista)
        {
            this.grafLista = grafLista;
            grafMacierz = new GrafMacierz(grafLista.LiczbaWierzcholkow);
            for (int i = 0; i < grafLista.LiczbaWierzcholkow; i++)
            {
                for (int j = 0; j < grafLista.Adj[i].Count; j++)
                {
                    grafMacierz.dodajKrawedz(new krawedz(i, grafLista.Adj[i][j]));
                }
            }

            listaPre = new List<int>();
            licznik = 0;
            grafMacierzA = new GrafMacierz(0);
            grafKosaraju = new GrafMacierz(0);
            for (int i = 0; i < grafLista.LiczbaWierzcholkow; i++)
            {
                listaPre.Add(-1);
                grafMacierzA.dodajWierzcholek();
                grafKosaraju.dodajWierzcholek();
            }
            
        }

        public void Wypisz()
        {
            grafMacierzA.Wypisz();
        }

        public void TCr(int v)
        {
            listaPre[v] = licznik++;
            List<int> B = grafMacierz.zdajSasiadow(v);
            for (int pozycja = 0; pozycja < B.Count(); pozycja++)
            {
                int t = B[pozycja];
                grafMacierzA.dodajKrawedz(new krawedz(v, t), true); 
                if (listaPre[v] < listaPre[t])
                    continue;
                if (listaPre[t] == -1)
                    TCr(t);
                for (int i = 0; i < grafMacierzA.Adj[t].Count; i++)
                    if (grafMacierzA.Adj[t][i].CzyIsteniejeKrawedz == true)
                        grafMacierzA.dodajKrawedz(new krawedz(v, i));
            }
        }


        public void TC()
        {
            for (int i = 0; i < grafMacierz.LiczbaWierzcholkow; i++)
            {
                if (listaPre[i] == -1)
                {
                    TCr(i);
                }
            }
        }

        public void Kosaraju()
        {

            nrSkladowej = new List<int>();
            int cnt = 0;
            for (int i = 0; i < grafMacierz.LiczbaWierzcholkow; i++)
                nrSkladowej.Add(0);
            for (int i = 0; i < grafMacierz.LiczbaWierzcholkow; i++)
            {
                List<int> iter = grafMacierz.zdajSasiadow(i);
                for (int j = 0; j < iter.Count(); j++)
                    grafKosaraju.dodajKrawedz(new krawedz(iter[j], i));
            }

            DFS dfs = new DFS(grafKosaraju);
            dfs.dfs();

            listaOrder = dfs.ListaTopologiczna;

            listaOrder.Reverse();

            DFS dfs2 = new DFS(grafMacierz);

            for (int t = 0; t < listaOrder.Count; t++)
            {
                if (dfs2.ListaPre[listaOrder[t]] == -1)
                {
                    dfs2.dfsr(listaOrder[t]);
                    cnt++;
                }
                nrSkladowej[t] = cnt;
            }
        }

        public void DAG_jadra()
        {
            grafDAG = new GrafMacierz(0);
            DAG kosaraju = new DAG(grafMacierz);
            kosaraju.Kosaraju();

            List<int> lista = new List<int>();
            for (int i = 0; i < grafMacierz.LiczbaWierzcholkow; i++)
                lista.Add(0);

            grafDAG.dodajVWierzcholkow(kosaraju.nrSkladowej[kosaraju.nrSkladowej.Count() - 1]);

            for (int i = 0; i < kosaraju.nrSkladowej.Count; i++)
                lista[kosaraju.listaOrder[i]] = kosaraju.nrSkladowej[i];

            for (int j = 0; j < lista.Count(); j++)
            {         
                List<int> iter = grafMacierz.zdajSasiadow(j);
                for (int k = 0; k < iter.Count(); k++)
                    if (lista[j] != lista[iter[k]])
                        grafDAG.dodajKrawedz(new krawedz(lista[j] - 1, lista[iter[k]] - 1));
            }
        }


    }
}
