﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace projektbart_omiejkaftancs
{
    interface IMetodyGrafowe
    {
        void dodajKrawedz(krawedz k, bool dezaktywator = false);
        void usunKrawedz(krawedz k);
        void dodajWierzcholek();
        void usunWierzcholek();
        void Wypisz();
        void generatorGrafu(int zadanaLiczbaKrawedzi);
        void generatorGrafu(double zadanaGestosc);
        bool czyJestKrawedzZVdoW(int v, int w);
        int maksymalnaLiczbaKrawedz();
    }
}
