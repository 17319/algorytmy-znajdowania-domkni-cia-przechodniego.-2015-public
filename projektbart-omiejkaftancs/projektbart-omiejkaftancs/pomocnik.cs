﻿using System;

namespace projektbart_omiejkaftancs
{
    public struct wierzcholek
    {
        private bool czyIsteniejeKrawedz;

        public wierzcholek(bool czyIsteniejeKrawedz)
        {
            this.czyIsteniejeKrawedz = czyIsteniejeKrawedz;
        }

        public bool CzyIsteniejeKrawedz
        {
            get
            {
                return czyIsteniejeKrawedz;
            }

            set
            {
                czyIsteniejeKrawedz = value;
            }
        }
    }
}

public struct krawedz
{
     int skad;
     int dokad;
    public krawedz(int skad = -1, int dokad = -1)
    {
        this.skad = skad;
        this.dokad = dokad;
    }

    public int Skad
    {
        get
        {
            return skad;
        }

        set
        {
            skad = value;
        }
    }

    public int Dokad
    {
        get
        {
            return dokad;
        }

        set
        {
            dokad = value;
        }
    }

    
}


